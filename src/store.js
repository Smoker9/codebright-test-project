import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

function getCurrentDate () {
  const date = new Date()
  const minutes = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()
  return `${date.getDate()}.${date.getMonth() + 1}.${date.getFullYear()} ${date.getHours()}:${minutes}`
}

export default new Vuex.Store({
  state: {
    postComments: [],
    currentCommentId: 1
  },
  mutations: {
    addComment (state, data) {
      const post = state.postComments.find(x => x.postId === data.postId)
      const newComment = {
        id: state.currentCommentId++,
        text: data.commentText,
        date: getCurrentDate()
      }

      if (post) {
        post.comments.push(newComment)
      } else {
        state.postComments.push({
          postId: data.postId,
          comments: [newComment]
        })
      }
    },
    editComment (state, data) {
      const post = state.postComments.find(x => x.postId === data.postId)

      if (post) {
        const comment = post.comments.find(x => x.id === data.commentId)
        if (comment) {
          comment.text = data.commentText
          comment.date = getCurrentDate()
        } else {
          alert('Cannot find comment')
        }
      }
    },
    deleteComment (state, data) {
      const post = state.postComments.find(x => x.postId === data.postId)
      if (post) {
        const commentIndex = post.comments.findIndex(x => x.id === data.commentId)

        if (commentIndex > -1) {
          post.comments.splice(commentIndex, 1)
        } else {
          alert('Cannot find comment')
        }
      }
    }
  },
  getters: {
    getComments: state => postId => {
      const post = state.postComments.find(x => x.postId === postId)
      if (post && post.comments) {
        return post.comments
      }
      return []
    },
    getCommentById: state => (postId, commentId) => {
      const post = state.postComments.find(x => x.postId === postId)
      if (post && post.comments) {
        return post.coments.find(x => x.id === commentId)
      }
      return null
    }
  },
  plugins: [createPersistedState()]
})
